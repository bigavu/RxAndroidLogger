package com.mysoftsource.rxandroidlogger.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mysoftsource.rxandroidlogger.TBPLogHelper;
import com.mysoftsource.rxandroidlogger.TPBLog;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    private Subscription mSubscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Timber.i("onCreate");

        findViewById(R.id.click_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TBPLogHelper.getInstance().storeAllPreviousLogToDropBox()
                        .toList()
                        .doOnSubscribe(() -> Log.d("MainActivity", "doOnSubscribe"))
                        .subscribe(paths -> {
                            StringBuilder builder = new StringBuilder();
                            builder.append("At: ");
                            for (String path : paths) {
                                builder.append(path);
                                builder.append(" |\n");
                            }
                            TPBLog.d( "Path saved: " + builder.toString());
                            Toast.makeText(getApplicationContext(), "Path saved: " +  builder.toString(), Toast.LENGTH_LONG).show();
                        }, throwable -> {
                            Toast.makeText(getApplicationContext(), "Store logcat is failed " + throwable.getMessage(), Toast.LENGTH_LONG);
                            TPBLog.e( throwable,"Store logcat is failed ");
                        });
            }
        });

        findViewById(R.id.dummy_log).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSubscription != null) {
                    return;
                }
                mSubscription = timer().subscribe();
            }
        });

        findViewById(R.id.cancel_log).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSubscription.unsubscribe();
                mSubscription = null;
            }
        });
    }

    private Observable<Long> timer() {
        return Observable.interval(10, TimeUnit.MILLISECONDS)
                .doOnNext(value -> Timber.d("dummy data %d", value));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.i("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.i("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Timber.i("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.i("onDestroy");
    }
}
