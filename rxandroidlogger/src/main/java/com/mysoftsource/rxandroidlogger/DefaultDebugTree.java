package com.mysoftsource.rxandroidlogger;

import android.content.Context;

public class DefaultDebugTree extends TBPDebugTree {
    protected DefaultDebugTree(Context context) {
        super(context);
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        super.log(priority, tag, message, t);
        printFile(tag, message, t);
    }
}
