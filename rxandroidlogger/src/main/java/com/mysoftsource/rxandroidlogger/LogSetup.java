package com.mysoftsource.rxandroidlogger;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;

public class LogSetup {
    @NonNull
    final String dropboxAccessToken;
    @NonNull
    final String serverFilePath;
    @Nullable
    final File localFilePath;
    final boolean isDeletePreviousFileLog;
    final boolean isOnlySaveFile;
    final long fileSizeMax;

    private LogSetup(@NonNull String dropboxAccessToken,
                     @NonNull String serverFilePath,
                     @Nullable File localFilePath,
                     boolean isDeletePreviousFileLog,
                     boolean isOnlySaveFile,
                     long fileSizeMax) {
        this.dropboxAccessToken = dropboxAccessToken;
        this.serverFilePath = serverFilePath;
        this.localFilePath = localFilePath;
        this.isDeletePreviousFileLog = isDeletePreviousFileLog;
        this.isOnlySaveFile = isOnlySaveFile;
        this.fileSizeMax = fileSizeMax;
    }

    public static class Builder {
        @NonNull
        String dropboxAccessToken;
        @NonNull
        String serverFilePath;
        @Nullable
        File localFilePath;
        boolean isDeletePreviousFileLog = false;
        boolean isOnlySaveFile = false;
        long fileSizeMax = DbHelper.MAXIMUM_FILE_SIZE;

        public Builder setDropboxAccessToken(String dropboxAccessToken) {
            this.dropboxAccessToken = dropboxAccessToken;
            return this;
        }

        public Builder setServerFilePath(String storePath) {
            this.serverFilePath = storePath;
            return this;
        }

        public Builder setLocalFilePath(File localFilePath) {
            this.localFilePath = localFilePath;
            return this;
        }

        public Builder setNeedDeletePreviousFileLog(boolean isDeletePreviousFileLog) {
            this.isDeletePreviousFileLog = isDeletePreviousFileLog;
            return this;
        }

        public Builder setOnlySaveFile(boolean isOnlySaveFile) {
            this.isOnlySaveFile = isOnlySaveFile;
            return this;
        }

        public Builder setMaxSizeMB(int maxSize) {
            this.fileSizeMax = maxSize * 1024 * 1024;
            return this;
        }

        public LogSetup build() {
            if (dropboxAccessToken == null) {
                throw new IllegalStateException("missing set dropbox access token");
            }
            if (serverFilePath == null) {
                throw new IllegalStateException("missing set dropbox path");
            }
            return new LogSetup(dropboxAccessToken,
                    serverFilePath,
                    localFilePath,
                    isDeletePreviousFileLog,
                    isOnlySaveFile,
                    fileSizeMax);
        }
    }
}
