package com.mysoftsource.rxandroidlogger;

import android.content.Context;

import timber.log.Timber;

abstract class TBPDebugTree extends Timber.DebugTree {
    private final Context mContext;

    protected TBPDebugTree(Context context) {
        mContext = context;
    }

    protected void printFile(String tag, String message, Throwable t) {
        StringBuilder builder = new StringBuilder(DateTimeUtil.getCurrentTime());
        builder.append(" ");
        builder.append(tag);
        builder.append(" ");
        builder.append(":");
        builder.append(" ");
        builder.append(message);
        builder.append(" ");
        builder.append(t != null ? t : "");

        DbHelper.getInstance().processSaveLog(builder.toString());
    }
}
