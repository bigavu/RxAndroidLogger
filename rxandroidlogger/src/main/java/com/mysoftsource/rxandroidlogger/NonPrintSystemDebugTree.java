package com.mysoftsource.rxandroidlogger;

import android.content.Context;

public class NonPrintSystemDebugTree extends TBPDebugTree {
    protected NonPrintSystemDebugTree(Context context) {
        super(context);
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        printFile(tag, message, t);
    }
}
